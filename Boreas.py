import re
import string
import json
import urllib2
from Logger import Logger

##  BOREAS
##  Parsing API
##
##  Onglao Industries
##  Enterprise Applications Division
##  GNU GPL 2013

class Manifest:
        ##  MANIFEST
        ##  Boreas Platform

        VERSION = "2.00.1001"
        BUILD = "BRS_RELEASE_JOF_31613.1"
        BUILD_DATE = "March 16, 2013"

        LICENSE = "GNU LGPL"
        KEY = "FG56X-U876A-GBN67-X098I"

        ##  Dependencies
	PARSER 	= {
			"APPLICATION": "Cat Doc", 
			"VERSION": "0.94.2", 
			"LICENSE": "GNU GPL", 
			"COPYRIGHT": "Victor Wagner www.wagner.pp.ru"
		}

	GEOLOCATION = {
			"APPLICATION": "Open Street Maps", 
			"VERSION": "1.0", 
			"LICENSE": "ODbL", 
			"COPYRIGHT": "OpenStreetMap Contributors www.openstreetmap.org/copyright"
		}
        
	LOG = {
			"APPLICATION": "Logger", 
			"VERSION": "1.00.1000", 
			"LICENSE": "LPGL", 
			"COPYRIGHT": "Onglao Industries"
		}

	DEPENDS = (PARSER, GEOLOCATION, LOG)


class Parser:
        ##  PARSER
        ##  Boreas Platform
        ##
        ##  This performs the parsing operations of the Boreas
        ##  platform. All binary and string operations
        ##  are computed here.
        
        __SILENT = False

        def __init__(self, Raw):        
                self.Log = Logger()
                self.Log.Trace("Boreas=>Parse::__init__(self, Raw)")

                if Raw.__len__() == 0:
                        self.Log.Error("Boreas=>Parse::__init__(): There is no raw data for the parser.")
                        raise Exception("There is no raw data for the parser.")
                else:
                        self.__RAW = Raw
                
                self.__AUX = Auxiliary()
                
                if not(self.__SILENT):
                        self.Log.SetTraceDisplay(Logger.DISPLAY) 

                self.Log.Trace("-------------------------------------------------------")
                self.Log.Trace("Boreas Parsing Platform")
                self.Log.Trace("version " + Manifest.VERSION)
                self.Log.Trace("(c) " + Manifest.VERSION + " Onglao Industries")
                self.Log.Trace("")
                self.Log.Trace("Linker to Argonaut Enterprise Platform")
                self.Log.Trace("Built for Journeys of Faith, Inc. and its subsidiaries.")
                self.Log.Trace("--------------------------------------------------------")
                
                if not(self.__SILENT):
                        self.Log.SetTraceDisplay(Logger.LOG_ONLY)
        
                self.Log.Trace("Log available at " + self.Log.GetLogPath())
        
        def SetRaw(self, Raw):
                self.Log.Trace("Boreas=>Parse::SetRaw(self, Raw)")
                
                if Raw.__len__() == 0:
                        self.Log.Error("Boreas=>Parse::SetRaw(): There is no raw data for the parser.")
                        raise Exception("There is no raw data for the parser.")
                else:
                        self.__RAW = Raw

        def ASCIICleanup(self, line):
                ##  ASCII CLEANUP
                ##
                ##  Cleans up any ASCII symbol to make the raw text
                ##  parse-able. This uses a configuration file
                ##  which extracts known symbols and replaces them
                ##  with a Boreas standard character. The configuration
                ##  file is updated to adapt to new symbols.
                ##
                ##  The user is informed of any new symbol, and would
                ##  require a substitute for that symbol. Symbols are
                ##  generally limited to what the Boreas standard
                ##  character map dictates
                
                self.Log.Trace("Boreas=>Parse::ASCIICleanup(self, line)")

                # Trim Extra Whitespace
                line = re.sub(r"\s+", " ", line)

                # Import Adaptive Cleanup Map Symbol Map
                # <code>

                # Standardize ASCII symbols
                for k, v in Library.Cleanup.iteritems():
                        line = line.replace(k, v)

                # Recursive cleanup
                if Library.RecursiveReplace in line:
                        line = line.replace(Library.RecursiveReplace, "\"")

                # Locate Unrecognized Symbols and Add Them To Symbol Map
                # <code>

                return line

        def Clean(self):
                ##  PARSE CLEANUP
                ##
                ##  Cleans up the ASCII characters not conforming to the
                ##  Boreas standard character map.
                ##
                ##  Removes any extraneous information that is not relevant
                ##  to extracting trips. These extraneous information are 
                ##  loaded from a configuration file. It looks for a starting
                ##  phrase and uses the end phrase to extract the extraneous
                ##  information in question. It then removes that whole string
                ##
                ##  The configuration file is also adaptable to unknown fragments
                ##  with the user specifying if the fragments are extraneous

                self.Log.Trace("Boreas=>Parse::Clean(self)")

                raw = self.ASCIICleanup(self.__RAW)

                Index = 0

                self.Log.Trace("Removing Extraneous Information")
                self.Log.Trace((str(Library.ExtraneousStart.__len__()) + " found"))

                for Tidbit in Library.ExtraneousStart:
                        StartSeq = 0
                        EndSeq = 0

                        StartSeq = raw.find(Tidbit)

                        if StartSeq > -1:
                                EndSeq = raw.find(Library.ExtraneousEnd[Index], StartSeq + Tidbit.__len__() + 1) + Library.ExtraneousEnd[Index].__len__()

                        self.Log.Trace(''.join(("Captured String: ", raw[StartSeq:EndSeq], "\n\t Start: ", Library.ExtraneousStart[Index], " (at ", str(StartSeq), ")\n\t End: ", Library.ExtraneousEnd[Index], " (at ", str(EndSeq), ")\n\t Length: ", str(EndSeq-StartSeq))))

                        raw = raw.replace(raw[StartSeq:EndSeq], "")
                        Index += 1

                self.__RAW = raw

        def Start(self):
                ##  START PARSER
                ##
                ##  Get the trips

                self.Log.Trace("Boreas=>Parse::Start(self)")
                
                raw = self.__RAW

                Trips = []
                HeadIndex = -1
                CurrentMonthIndex = 0

                MonthRecurrTable = self.__AUX.LocateMonth(raw);
                        
                #   Reverse iteration: Find the last month that has non-zero elements
                LastMonth = 0
                Index = 0
                for Month in reversed(MonthRecurrTable):
                        if Month.__len__() > 0:
                             LastMonth = 11 - Index
                             break

                        Index += 1
                
                self.Log.Trace("LastMonth: {0}".format(LastMonth))

                #   Assume Chronological Order for Trips
                for Month in Library.Months:
                        MonthIndex = MonthRecurrTable[CurrentMonthIndex]

                        #print "MONTH INDEX: ", MonthIndex
                        # If we find a match for this month, find the day
                        if MonthIndex.__len__() > 0:
                                for i in range(MonthIndex.__len__()):

                                        if Month == Library.Months[LastMonth]:    
                                                self.Log.Trace("Reached end of Trip Data.")

                                                # Push to the list of trips 
                                                Trips.append(Trip(raw[0:raw.__len__()]))
                                        else:
                                                if i != MonthIndex.__len__() - 1:
                                                        # Push to the list of trips
                                                        #print "TRY: ", MonthIndex[i]
                                                        #print "APPEND: ", raw[MonthIndex[i]:MonthIndex[i+1]]
                                                        Trips.append(Trip(raw[MonthIndex[i]:MonthIndex[i+1]]))

                                raw = raw[MonthIndex[MonthIndex.__len__() - 1]:raw.__len__()]
                                
                                # Grab the last remaining trip data
                                PickupTrip = False
                                
                                for Pickup in Month:
                                        # Determine if we have to pick up the remaining trip that has been
                                        # left off during the excavation.

                                        # Starts with via the list will give a good approximation. Dedicated to Won Kim
                                        if raw.startswith(Pickup + " ") or raw.startswith(" " + Pickup + " "):
                                                PickupTrip = True
                                                break

                                if PickupTrip:
                                        # Adjust Raw Data to account for this trip's end date as the next month
                                        #   E.g. Feb - Mar EVENT; Having March as the end date will render the parse inaccurate
                                        
                                        # If the index are the same
                                        # The earliest index of the next trip from the tuple list is indeed the bounds of the current trip
                                        TripBoundaryIndices = []
                                        StartFindIndex = self.__AUX.ShiftTripIndex(raw) 
                                        
                                        if CurrentMonthIndex < Library.Months.__len__() - 1:
                                                for MonthAnt in Library.Months[CurrentMonthIndex + 1]:
                                                        # Find the closests index for the next trip based on identifiers
                                                        PokeBoundaryIndex = raw.find(MonthAnt, StartFindIndex) 
                                                
                                                        # Prevent duplicates and ensure that the match has been found
                                                        if not(PokeBoundaryIndex in TripBoundaryIndices) and PokeBoundaryIndex > -1:
                                                                TripBoundaryIndices.append(PokeBoundaryIndex)

                                                # Sort to get the minimum value (which will be the next identifier) 
                                                TripBoundaryIndices.sort()
                                                
                                                if TripBoundaryIndices.__len__() > 0:
                                                        # Push to the list of trips
                                                        Trips.append(Trip(raw[0:TripBoundaryIndices[0]]))

                                                        # Update the raw data
                                                        raw = raw[TripBoundaryIndices[0]:raw.__len__()]
                                
                                #   Update Indices based on new raw data
                                MonthRecurrTable = self.__AUX.LocateMonth(raw)

                        CurrentMonthIndex += 1

                                
                return Trips

        def PrintAllTrips(self, TripList):
                Index = 0

                for Trip in TripList:
                        #self.Log.Trace(("[" + Index + "] = " + Trip.Raw()))
                        ThisTrip = Trip.Data()

                        print """====[Trip {0}]==================================== 
				RAW: {1}
				START: {2} 
				END: {3} 
				TITLE: {4}
				LOCATIONS: {5}
				===================================================\n""".format(Index, Trip.Raw(), ThisTrip['Date']['Start'], 
						ThisTrip['Date']['End'], ThisTrip['Title'], ThisTrip['Locations'])
                        Index += 1

class Auxiliary:
        ##  AUXILIARY
        ##  Boreas Platform
        ##
        ##  Auxiliary functions that are not attached to any class
        ##  during instantiation. These functions serve as a common
        ##  staging point for both internal and external API usage.
        ##
        ##                           ---> External
        ##  BOREAS ---> AUXILIARY --|
        ##                           ---
        ##                              |                              
        ##                 Internal <---
        ##

        ##--====================================================================
        ## JUST A THOUGHT: Separate Auxiliary functions into actual designated functions
        ##      Such as IsMonth into a class for Date.
        ##=======================================================================

        def __init__(self):
                self.Log = Logger();

        def ShiftTripIndex(self, str):
                ##  Set the next logical trip search index via tokens
                
                _Tokens = str.split()

                tokenIndex = 0
                returnIndex = 0

                # Do spatial logic analysis to see if the months are spaced enough
                # to detect the same trip, or another trip.
                if _Tokens.__len__ > 0:
                        while True:
                                thisToken = _Tokens[tokenIndex]

                                if tokenIndex == 0:
                                        if self.IsMonth(thisToken):
                                                # + 1 accounts for space after the token
                                                returnIndex += thisToken.__len__() + 1
                                        else:
                                                break
                                else:
                                        if self.IsInt(thisToken) or "-" in thisToken or self.IsMonth(thisToken):
                                                # + 1 accounts for space after the token
                                                returnIndex += thisToken.__len__() + 1
                                        else:
                                                break

                                tokenIndex += 1

                return returnIndex

        #   Create table of months and number of occurrences
        def LocateMonth(self, raw):
                ##  Locate the recurrence of months via index.
                ##  Includes all months in the tuple definition

                MonthRecurrTable = [];
                
                for Month in Library.Months:
                        # Build up mutable list of iterable indices
                        MonthIndex = []
                        for MonthAnt in Month:
                                # LOOK FOR BETTER IMPLEMENTATION: search with all tuple elements
                                TempIndex = [match.start() for match in re.finditer(re.escape((MonthAnt + " ")), raw)]
                        
                                # Include all possible indices for this month
                                for i in TempIndex:
                                        if not(i in MonthIndex):
                                                MonthIndex.append(i)

                        MonthIndex.sort()

                        MonthRecurrTable.append(MonthIndex)
                
                #print MonthRecurrTable
                return MonthRecurrTable
        
        def IsDateValid(self, Start, End):
                ##  Check if the start date goes before the end date
                
                if End.Year >= Start.Year:
                        if End.Month >= Start.Month:
                                if Start.MonthOnly and End.MonthOnly:
                                        return True
                                else:
                                        if End.Day >= Start.Day:
                                                return True
                                        else:
                                                return False
                        else:
                                return False
                else:
                        return False
                                

        def IsMonth(self, Month):
                ##  Check if the Month is valid

                #self.Log.Trace("Date: {0}".format(Month))
                for MonthTuple in Library.Months:
                        if Month in MonthTuple:
                                return True
                                break
                return False

        def MonthIndex(self, Month):
                ##  Get the index of the month
                
                Index = 1

                for MonthTuple in Library.Months:        
                        if Month in MonthTuple:
                                break
                        else:
                                Index += 1
        
                return Index

        def IsInt(self, str):
                ##  Check if the string is an integer

                try:
                        n = int(str)
                        return True
                except ValueError:
                        return False

                return False


        # Signal if capturing title is done
        TitleDone = False
        #   JUST A THOUGHT: Aggregate into a class, and use Done() method just like in Locate class
        def IsTitle(self, Title):
                #   Determine if the title is all uppercase (or uses ampersand &) 
                #   Perform Converse Logic. Instead of looking for all uppercase letters,
                #   we will halt if a lowercase letter is detected
                #
                #   Limit to locale-independent alpha characters
                
                #self.Log.Console("Title: {0}".format(Title))

                if not(self.TitleDone):
                        if Title.isupper() or '&' in Title:
                                self.Log.Trace("Title: {0}".format(Title))
                                return True
                        else:
                                self.Log.Trace("Title: {0}     !!!!!! <VALID> !!!!!!!".format(Title))
                                return False
                else:
                        return False

        def TitleCase(self, s):
                return re.sub(r"[A-Za-z]+('[A-Za-z]+)?", lambda mo: mo.group(0)[0].upper() + mo.group(0)[1:].lower(),s)


class Library:
        ##  LIBRARY
        ##  Boreas Platform
        ##
        ##  Contains all the necessary constants and data for parsing

        ## Months and Associated Identifiers
        ##  Increased performance: keep all lowercase 
        ##  so that the number of calls will be reduced
        
        Months = [  ('JAN', 'JANUARY'), \
                    ('FEB', 'FEBRUARY'),\
                    ('MAR', 'MARCH'),\
                    ('APR', 'APRIL'),\
                    ('MAY', 'MAY'),\
                    ('JUN', 'JUNE'),\
                    ('JUL', 'JULY'),\
                    ('AUG', 'AUGUST'),\
                    ('SEPT', 'SEPTEMBER'),\
                    ('OCT', 'OCTOBER'),\
                    ('NOV', 'NOVEMBER'),\
                    ('DEC', 'DECEMBER')]
        
        Year = 2013

        ## ASCII Cleanup Parameters
        Cleanup = {"& more": "", "(optional extension)": "", "  -  ": " - ", "`": "'", ",,": ",", "''": "\"", "-": " - ", "w/": "with ", "plus optional": "with optional", "with optional pre-extension": "with optional extension"}      # Key-Value Hash; Key for needle; Value for replaced value
        RecursiveReplace = "''"                                                     # List of Key (needle) values to trigger recursive cleanup

        ## Remove Extraneous
        ExtraneousStart = ["YOU ARE INVITED", "\"Let me show you", "We also customize"]
        ExtraneousEnd = [")", "Arlina", "www.journeys.com.ph"]

        ## Find Last Date Updated
        LastModifiedStart = "(Updated"
        LastModifiedEnd = ")"
        LastModifiedDate = ""

        ## Token Capture Mode
        CAPTURE_NONE = 0
        CAPTURE_DATE = 1
        CAPTURE_TITLE = 2
        CAPTURE_LOCATION = 3
        CAPTURE_PEOPLE = 4
        CAPTURE_OPTIONAL = 5

class Trip:
        ##  TRIP
        ##  Boreas Platform
        ##
        ##  A class that contains the trip information
        ##  which can also contain linked files for full
        ##  detailed trip information.

        ##  Raw trip
        __RawInfo = ""

        def __init__(self, RawInfo):
                ##  Begin Parsing
                if RawInfo.__len__() > 0:
                        self.Log = Logger()
                        self.Locate = Locate()
                        self._AUX = Auxiliary() 

                        ##  Initialize Data
                        self.Start = {'Month': 0, 'Day': 0, 'Year': 0, 'MonthOnly': False}
                        self.End = {'Month': 0, 'Day': 0, 'Year': 0, 'MonthOnly': False}
                        self.Title = ""
                        self.Locations = {}
                        self.People = []
                        self.Optional = {'TID': 0, 'Title': "", 'Start': {'Month': 0, 'Day': 0, 'Year': 0}, 'End': {'Month': 0, 'Day': 0, 'Year': 0}}
                        self.__RawInfo = RawInfo
                        self.Parse()
                else:
                        raise Exception("There is not trip data")
        
        def Data(self):
                return {"Title": self._AUX.TitleCase(self.Title), "Locations": self.Locations, "People": self.People, "Date": {"Start": self.Start, "End": self.End}, "Optional": self.Optional}

        def Raw(self):
                return self.__RawInfo

        ##  PARSE ENGINE
        ##  Split information into specified bins
        def Parse(self):
                _Tokens = self.__RawInfo.split()
                _TokenIndex = 0

                Capture = Library.CAPTURE_NONE              
                State = 0

                RecapturedToken = ""
                TokenOperations = []

                if _Tokens.__len__() > 0:
                        for _Token in _Tokens:
                                # Instantiate to only one token
                                TokenOperations = [_Token]

                                # If a token has been recaptured, include into <TokenOperations>
                                if RecapturedToken:
                                        TokenOperations.insert(0, RecapturedToken)
                                        RecapturedToken = ""

                                # Perform Token Operations
                                for _Token in TokenOperations:
                                        #   Determine Capture Mode
                                        if Capture == Library.CAPTURE_NONE:
                                                # Reset State
                                                State = 0

                                                if self._AUX.IsMonth(_Token):
                                                        self.Log.Trace("CAPTURE_DATE... Token = {0}".format(_Token))
                                                        self.Start['Month'] = self._AUX.MonthIndex(_Token)
                                                        Capture = Library.CAPTURE_DATE
                                                elif self._AUX.IsTitle(_Token):
                                                        if '&' in _Token:
                                                                self.Log.Warning("Parse => Title: Trip title begins with '&'.") 
                                                        else:
                                                                self.Title = _Token  

                                                        Capture = Library.CAPTURE_TITLE
                                                elif self.Locate.is_valid(_Token):
							self.Locate.set_temp(_Token)
                                                        Capture = Library.CAPTURE_LOCATION
                                                
                                        #   CAPTURE: Title
                                        #     Token State Machine Progression
                                        elif Capture == Library.CAPTURE_TITLE:
                                                self.Log.Trace("CAPTURE_TITLE... Token = {0}".format(_Token))
                                                
                                                if State == 0:
                                                        self.Log.Trace("State {0}".format(State))
                                                        if self._AUX.IsTitle(_Token):
                                                                self.Title = ''.join([self.Title, " ", _Token])
                                                                State = 0
                                                        else:
                                                                self._AUX.TitleDone = True
                                                                RecapturedToken = _Token
                                                                Capture = Library.CAPTURE_NONE      # END: Title 
                                                else:
                                                        Capture = Library.CAPTURE_NONE
                                        #   CAPTURE: Locations
                                        #     Token State Machine Progression
                                        elif Capture == Library.CAPTURE_LOCATION:
                                                self.Log.Trace("CAPTURE_LOCATION... Token = {0}".format(_Token))
                                                
                                                if State == 0:
                                                        if self.Locate.is_valid(_Token):
                                                            	
								#print "TEMP: ", self.Locate.get_temp()
								
#								if "-" in _Token:
#									# Next Feature: Check if succeeding tokens make sense 
#									print "+1 Country: ", self.Locate.get_temp()
#									self.Locate.add_country(self.Locate.get_temp())
#									self.Locate.set_temp(_Token)	
#								#elif "(" in _Token and ")" in _Token:
#								#	# Add this city is delimiter is found
#								#	print "+ City x (in ", self.Locate.this_country, "): ", _Token
#								#	self.Locate.add_city(self.Locate.this_country, _Token)
#
#								elif "(" in _Token:
#									#self.Locate.set_temp(_Token)
#									
#									self.Locate.this_country = self.Locate.get_temp()
#									print "+2 Country: ", self.Locate.get_temp()
#									self.Locate.add_country(self.Locate.get_temp())
#									self.Locate.set_temp(_Token)
#
#									if "," in _Token:
#										# Add this city is delimiter is found
#										print "+3 City (in ", self.Locate.this_country, "): ", _Token
#										self.Locate.add_city(self.Locate.this_country, _Token)
#										#self.Locate.set_temp(_Token)
#
#									elif ")" in _Token:
#										# If done with country, empty country temp var, and commit
#										# the rest as a city of that country
#										print "+4 City (in ", self.Locate.this_country, "): ", _Token
#										#print "+ x2 City (in ", self.Locate.this_country, "): ", self.Locate.get_temp()
#										#self.Locate.add_city(self.Locate.this_country, self.Locate.get_temp())
#										self.Locate.add_city(self.Locate.this_country, _Token)
#										self.Locate.this_country = ""
#										
#								else:
#									#self.Locate.set_temp(_Token)
#									
#									if self.Locate.this_country:
#										if "," in _Token:
#											self.Locate.set_temp(_Token)
#											print "+5 City (in ", self.Locate.this_country, "): ", self.Locate.get_temp()
#											self.Locate.add_city(self.Locate.this_country, self.Locate.get_temp())
#										
#										elif ")" in _Token:
#											# If done with country, empty country temp var, and commit
#											# the rest as a city of that country
#											self.Locate.set_temp(_Token)
#											print "+6 City (in ", self.Locate.this_country, "): ", self.Locate.get_temp()
#											self.Locate.add_city(self.Locate.this_country, self.Locate.get_temp())
#											self.Locate.this_country = ""
#
#										else:
#											self.Locate.set_temp(_Token)
#
								State = 0
									
								# Keep Appending
								self.Locate.set_temp(_Token)

                                                        else:       
								# And we are done!
								self.Locations = self.Locate.done()
                                                                self.Locate.clearup()
								Capture = Library.CAPTURE_NONE
                                                else:
                                                        Capture = Library.CAPTURE_NONE        
                                        #   CAPTURE: Date
                                        #     Token State Machine Progression 
                                        elif Capture == Library.CAPTURE_DATE:
                                                self.Log.Trace("Token = {0}".format(_Token))

                                                if State == 0:
                                                        self.Log.Trace("State {0}".format(State))
                                                        if self._AUX.IsInt(_Token):
                                                                # If we have a year, end
                                                                if int(_Token) >= Library.Year:
                                                                        self.Start['Year'] = int(_Token)
                                                                        self.End['Year'] = int(_Token)
                                                                        self.Start['MonthOnly'] = True
                                                                        self.End['MonthOnly'] = True
                                                                        self.End['Month'] = self.Start['Month']
                                                                        self.Log.Trace("<END> Start: {0}, End: {1}".format(self.Start, self.End))
                                                                        Capture = Library.CAPTURE_NONE  # END: MM YYYY (other year)
                                                                else:
                                                                        self.Start['Day'] = int(_Token)
                                                                        State = 1
                                                        elif "-" in _Token:
                                                                self.Start['MonthOnly'] = True
                                                                State = 3
                                                        else:
                                                                self.Start['Year'] = Library.Year
                                                                self.End['Year'] = Library.Year
                                                                self.End['Month'] = self.Start['Month']
                                                                self.Start['MonthOnly'] = True
                                                                self.End['MonthOnly'] = True
                                                                self.Log.Trace("<END> Start: {0}, End: {1}".format(self.Start, self.End))
                                                                RecapturedToken = _Token
                                                                Capture = Library.CAPTURE_NONE  # END: MM YYYY (current year)

                                                elif State == 1:
                                                        self.Log.Trace("State {0}".format(State))
                                                        if "-" in _Token:
                                                                State = 2
                                                        elif self._AUX.IsInt(_Token):
                                                                if int(_Token) > Library.Year:
                                                                        self.Start['Year'] = int(_Token)
                                                                        State = 1
                                                                else:        
                                                                        self.End['Month'] = self._AUX.MonthIndex(_Token)
                                                                        State = 3
                                                        else:
                                                                self.Log.Error("Parse => Date: Invalid State Machine Progression after State 1")

                                                elif State == 2:
                                                        self.Log.Trace("State {0}".format(State))
                                                        if self._AUX.IsInt(_Token):
                                                                self.End['Month'] = self.Start['Month']
                                                                self.End['Day'] = int(_Token)
                                                                self.Start['Year'] = Library.Year   
                                                                self.End['Year'] = Library.Year     
                                                                self.Log.Trace("<END> Start: {0}, End: {1}".format(self.Start, self.End))
                                                                Capture = Library.CAPTURE_NONE      # END: MM DD - DD YYYY (current year)
                                                        elif self._AUX.IsMonth(_Token):
                                                                self.End['Month'] = self._AUX.MonthIndex(_Token)
                                                                State = 5
                                                        else:
                                                                self.Log.Error("Parse => Date: Invalid State Machine Progression after State 2")

                                                elif State == 3:
                                                        self.Log.Trace("State {0}".format(State))
                                                        if self._AUX.IsMonth(_Token):
                                                                self.End['Month'] = self._AUX.MonthIndex(_Token)
                                                                self.End['MonthOnly'] = True
                                                                State = 4
                                                        else:
                                                                self.Log.Error("Parse => Date: Invalid State Machine Progression after State 3")
               
                                                elif State == 4:
                                                        self.Log.Trace("State {0}".format(State))
                                                        if self._AUX.IsInt(_Token):
                                                                if int(_Token) >= Library.Year:
                                                                        self.Start['Year'] = Library.Year
                                                                        self.End['Year'] = int(_Token)     
                                                                        self.Log.Trace("<END> Start: {0}, End: {1}".format(self.Start, self.End))
                                                                        Capture = Library.CAPTURE_NONE  # END: MM (YYYY current) - MM YYYY (other year); e.g. DEC - JAN 2014
                                                                else:
                                                                        self.Log.Error("Parse => Date: Invalid State Machine Progression after State 4")
                                                        else:
                                                                self.Log.Error("Parse => Date: Invalid State Machine Progression After State 4")

                                                elif State == 5:
                                                        self.Log.Trace("State {0}".format(State))
                                                        if self._AUX.IsInt(_Token):
                                                                if int(_Token) >= Library.Year:
                                                                        self.End['Year'] = int(_Token)
									self.Log.Trace("<END> Start: {0}, End: {1}".format(self.Start, self.End))
                                                                        Capture = Library.CAPTURE_NONE  # END: MM DD - MM DD YYYY (other year) <or> MM DD YYYY (current year) - MM DD YYYY (other year)
                                                                else:
                                                                        self.End['Day'] = int(_Token)
                                                                        State = 5
                                                        else:
                                                                self.Start['Year'] = Library.Year
                                                                self.End['Year'] = Library.Year
                                                                RecapturedToken = _Token
                                                                self.Log.Trace("<END> Start: {0}, End: {1}".format(self.Start, self.End))
                                                                Capture = Library.CAPTURE_NONE          # END: MM DD - MM DD (current year)
                                        
                else:
                        raise Exception("The trip has no usable tokens.")


class Locate:
        ##  LOCATE
        ##  Boreas Platform
        ##
        ##  Locates a place (city, town, or country) through the internet
        ##  via search engine results. Successful finds are classified
        ##  either as city, town, or country. Geolocation coordinates
        ##  such as Latitude and Longitude are also determined.
        ##
        ##  System will try to do auto-correct for places
        _except = ("Fr.", "Bishop", "with", "w/")
        
	_country_markers = ("-", "(")
	_city_markers = (",")

	_remove_symbols = ("-", "(", ")", ",")

	# temporary variable
	_this_location = ""

	this_country = ""

	_done = False
	_locations = {}

        def __init__(self):
		self.Log = Logger()
        
        def is_valid(self, Location):
                if not(self._done):
                        self.Log.Trace("Location: {0}".format(Location))

                        if not(Location in self._except):
                              	# Gather information from http://nominatim.openstreetmap.org/
                                # Query results are streamed through JSON
                                #Raw = urllib2.urlopen('http://nominatim.openstreetmap.org/search?format=json&addressdetails=1&q={0}'.format(Location))
                                #LocData = json.load(Raw)
                                #if LocData.__len__() > 0:
                                        #self.Log.Console(LocData[0]['display_name'])             
                        
                                return True
                        else:
                                return False
                else:
                        return False
	
	def add_country(self, country):
		country = self.format_location(country)
		self._locations[country] = []
		self.clear_temp()
		
	def add_city(self, country, city):
		city = self.format_location(city)
		self._locations[country].append(city)
        	self.clear_temp()

	def set_temp(self, token):
		if self._this_location:
			self._this_location = "{0} {1}".format(self._this_location, token)
		else:
			self._this_location = token

		self._this_location = self.format_location(self._this_location)

	def get_temp(self):
		return self._this_location

	def clear_temp(self):
		self._this_location = ""

	def format_location(self, raw):
		for symbol in self._remove_symbols:
			#print "SYMBOL = ", symbol
			raw = raw.replace(symbol, "")

		return raw

	def clearup(self):
		self._locations.clear()

	def done(self):
                self._done = True
		return self._locations

