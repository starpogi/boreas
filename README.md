# Project Boreas #

Still has issues and doesn't work yet.

Parses itineraries into a unified data structure. This will harvest information from documents and allow other applications to access or read the data.

Parsing engine uses Lexical Token Analysis algorithm that treats every piece of information as a token. 

Used for Journeys of Faith, Inc. (my mom's company) to streamline productivity in updating the website from the Word documents she sends out.

### Usage ###
Running the project involves piping a converted document into the Python script. For example:
 
    :::python
         import sys
         from Boreas import Parser, Library
         from Logger import Logger

         RawTrip = ""
 
         for line in sys.stdin:
                 RawTrip = ''.join([RawTrip, "", line])

         ##  Clean up the raw parsed data, and generate a more elegant
         ##  and cleaner parsable data.
         Parse = Parser(RawTrip)
         Parse.Clean()
         Trips = Parse.Start()
 
         Parse.PrintAllTrips(Trips)

Invoke the parser in the shell via

    $ catdoc Invitation\ \(colored\).doc | python main.py


`Catdoc` is the default DOC to TXT converter. Any other converters can be used. Furthermore, `Invitation\ \(colored\).doc` refers to any document targeting the Parser.

### Version Log ###

1.00.1000

* API development
* Included Catdoc for Microsoft Word document parsing into plain text
* Basic Parsing operations: extracting raw trip information, bucket drop into bins

1.00.1100

* Enhanced raw trip parsing algorithm: Multi-nodal State Machine Lexical Analysis
* Separated Logger from Boreas
* Open Street Maps Database integration with geolocation
* Code restructing for modularization

2.00.1001

* Plans to improve algorithm using Drop Bucket Parsing Tree
* Plans to include Hadoop BSON database for caching geolocation data for trip locations
* Plans to lookup geolocation data for trip locations for user-interactive applications
* Parse=>GetTrips replaced to Parse=>Start
* Enhancement of data accessor methods (speed up or simplification)