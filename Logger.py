from time import localtime, strftime
from datetime import datetime
import platform
import os

class Logger:
        ##  LOGGER
        ##  
        ##  This allows any platform to log important events
        ##  into a log file that is named by the time of the 
        ##  main function execution. Also, this has a feature that
        ##  allows rapid debugging by allowing log messages to be
        ##  displayed on console.
        ##
        ##  Messages are categorized as the following:
        ##      Trace - Messages that serve as informational such
        ##              as function calls, and variable declarations.
        ##      Error - Fatal or semantic errors that causes the main
        ##              program to not work as expected.
        ##      Warning - May cause semantic errors, but program can
        ##              still work as expected
        
        ## Scope Constant Variables
        ##  These variables control display method to developer
        ##  or user.
        LOG_ONLY = 0
        DISPLAY = 1
        DISPLAY_ONLY = 2

        __LogFile = ""

        __TraceDisplay = LOG_ONLY
        __ErrorDisplay = DISPLAY
        __WarningDisplay = LOG_ONLY

        __LOG_ENABLED = False

	__VERSION = "1.00.0000"

        def __init__(self):
                if self.__LOG_ENABLED == True:
                        self.__LogFile = strftime("Logs/Boreas-Logs-%Y-%m-%d-%H-%M-%S.txt", localtime())
			self.LogInformation()
                else:
                        self.__LogFile = "Log Disabled"

	def LogInformation(self):
		if self.__LOG_ENABLED == True:
                        fp = open(self.__LogFile, "w")
			LoggerInformation = """Logger {0}\nGreco Generation\n\n
						Platform: \t{1}\n
						System: \t{2}\n
						Release: \t{3}\n
						Version: \t{4}\n
						OS: \t{5}\n\n"""
			
			LoggerInformation = LoggerInformation.format(self.__VERSION, platform.platform(), platform.system(), platform.release(), platform.version(), os.name) 
                        
			fp.write(LoggerInformation)
                        fp.close()               


        def LogToFile(self, log_message, console_message, display):
                if self.__LOG_ENABLED == True and display < self.DISPLAY_ONLY:
                        fp = open(self.__LogFile, "a")
                        fp.write(str(datetime.now().strftime("%Y-%m-%d||%H:%M:%S.%f||") +log_message + "\n"))
                        fp.close()               
                
                if display > self.LOG_ONLY:
                        print console_message

        def GetDisplayMode(self, display):
                if display == self.LOG_ONLY:
                        return "LOG_ONLY"
                elif display == self.DISPLAY:
                        return "DISPLAY"
                elif display == self.DISPLAY_ONLY:
                        return "DISPLAY_ONLY"
                else:
                        return "UNKNOWN"

        def GetLogPath(self):
                return self.__LogFile

        def SetTraceDisplay(self, display):
                self.__TraceDisplay = display

        def SetErrorDisplay(self, display):
                self.__ErrorDisplay = display

        def SetWarningDisplay(self, display):
                self.__WarningDisplay = display

        def GetTraceDisplay(self):
                return self.GetDisplayMode(self.__TraceDisplay)

        def GetErrorDisplay(self):
                return self.GetDisplayMode(self.__ErrorDisplay)

        def GetWarningDisplay(self):
                return self.GetDisplayMode(self.__WarningDisplay)

        def Trace(self, message):
                self.LogToFile(''.join(("TRACE||", message)), message, self.__TraceDisplay)

        def Warning(self, message):
                self.LogToFile(''.join(("WARNING||", message)), message, self.__WarningDisplay)

        def Error(self, message):
                self.LogToFile(''.join(("ERROR||", message)), message, self.__ErrorDisplay)

        def Console(self, message):
                print message
