import re
import sys
from time import time
from datetime import timedelta

from Boreas import Parser, Library
from Logger import Logger

##  BOREAS
##  Parser
##
##  Onglao Industries
##  Enterprise Applications Division
##  GNU GPL 2013

## Trip Statistics
Trips = []

## Main Implementation
def main():      
        Parse()
        end = time()
        elapsed = end-start
        print elapsed

def Parse():
        ##  PARSE
        ##
        ##  This method attempts to parse the raw information received
        ##  from the standard input pipe. The data is then extracted
        ##  assuming that the files follow the standard document
        ##  
        BoreasLog.Trace(__name__)
        BoreasLog.Trace("BOREAS::Parse")
        BoreasLog.Trace("Begin Parsing...")

        ##  Combine Standard Console Input via piping into one line
        ##  This assumes that the data being read does not exceed
        ##  500 KB in size. Increases in size will affect virtual
        ##  paging and heap performance.
        RawTrip = ""
						
        for line in sys.stdin:                
                RawTrip = ''.join([RawTrip, "", line])

        ##  Clean up the raw parsed data, and generate a more elegant
        ##  and cleaner parsable data. 
        Parse = Parser(RawTrip)
        Parse.Clean()
        Trips = Parse.Start()
        
        #Parse.PrintAllTrips(Trips)

        BoreasLog.Console((str(Trips.__len__()) + " trip(s) found."))

if __name__ == '__main__':
        start = time()
        BoreasLog = Logger()

        main()
